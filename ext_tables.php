<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntBsBtn',
            'Hivecptcntbsbtnrender',
            'Button or Button Group'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntBsBtn',
            'Hivecptcntbsbtndocumentation',
            'Button :: User documentation'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_cpt_cnt_bs_btn', 'Configuration/TypoScript', 'hive_cpt_cnt_bs_btn');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntbsbtn_domain_model_btn', 'EXT:hive_cpt_cnt_bs_btn/Resources/Private/Language/locallang_csh_tx_hivecptcntbsbtn_domain_model_btn.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntbsbtn_domain_model_btn');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntbsbtn_domain_model_btngroup', 'EXT:hive_cpt_cnt_bs_btn/Resources/Private/Language/locallang_csh_tx_hivecptcntbsbtn_domain_model_btngroup.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntbsbtn_domain_model_btngroup');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntbsbtn_domain_model_documentation', 'EXT:hive_cpt_cnt_bs_btn/Resources/Private/Language/locallang_csh_tx_hivecptcntbsbtn_domain_model_documentation.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntbsbtn_domain_model_documentation');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

$extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY));
$pluginName = strtolower('Hivecptcntbsbtnrender');
$pluginSignature = $extensionName.'_'.$pluginName;
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'.$_EXTKEY . '/Configuration/FlexForms/Config.xml');

plugin.tx_hivecptcntbsbtn_hivecptcntbsbtnrender {
    view {
        templateRootPaths.0 = EXT:hive_cpt_cnt_bs_btn/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hivecptcntbsbtn_hivecptcntbsbtnrender.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_cpt_cnt_bs_btn/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hivecptcntbsbtn_hivecptcntbsbtnrender.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_cpt_cnt_bs_btn/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hivecptcntbsbtn_hivecptcntbsbtnrender.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hivecptcntbsbtn_hivecptcntbsbtnrender.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_hivecptcntbsbtn_hivecptcntbsbtndocumentation {
    view {
        templateRootPaths.0 = EXT:hive_cpt_cnt_bs_btn/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hivecptcntbsbtn_hivecptcntbsbtndocumentation.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_cpt_cnt_bs_btn/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hivecptcntbsbtn_hivecptcntbsbtndocumentation.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_cpt_cnt_bs_btn/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hivecptcntbsbtn_hivecptcntbsbtndocumentation.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hivecptcntbsbtn_hivecptcntbsbtndocumentation.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_hivecptcntbsbtn._CSS_DEFAULT_STYLE (
        textarea.f3-form-error {
                background-color:#FF9F9F;
                border: 1px #FF0000 solid;
        }

        input.f3-form-error {
                background-color:#FF9F9F;
                border: 1px #FF0000 solid;
        }

        .{extension.cssClassName} table {
                border-collapse:separate;
                border-spacing:10px;
        }

        .{extension.cssClassName} table th {
                font-weight:bold;
        }

        .{extension.cssClassName} table td {
                vertical-align:top;
        }

        .typo3-messages .message-error {
                color:red;
        }

        .typo3-messages .message-ok {
                color:green;
        }
)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin.tx_hivecptcntbsbtn_hivecptcntbsbtnrender {
    settings {
        bDebug = {$plugin.tx_hivecptcntbsbtn_hivecptcntbsbtnrender.settings.bDebug}
        sDebugIp = {$plugin.tx_hivecptcntbsbtn_hivecptcntbsbtnrender.settings.sDebugIp}
    }
}

plugin {
    tx_hivecptcntbsbtn {
        model {
            HIVE\HiveCptCntBsBtn\Domain\Model\Btn {
                persistence {
                    storagePid = {$plugin.tx_hivecptcntbsbtn.model.HIVE\HiveCptCntBsBtn\Domain\Model\Btn.persistence.storagePid}
                }
            }
        }
    }
}
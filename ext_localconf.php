<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntBsBtn',
            'Hivecptcntbsbtnrender',
            [
                'Btn' => 'render'
            ],
            // non-cacheable actions
            [
                'Btn' => 'render'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntBsBtn',
            'Hivecptcntbsbtndocumentation',
            [
                'Documentation' => 'render'
            ],
            // non-cacheable actions
            [
                'Documentation' => 'render'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivecptcntbsbtnrender {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_cpt_cnt_bs_btn') . 'Resources/Public/Icons/user_plugin_hivecptcntbsbtnrender.svg
                        title = LLL:EXT:hive_cpt_cnt_bs_btn/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_bs_btn_domain_model_hivecptcntbsbtnrender
                        description = LLL:EXT:hive_cpt_cnt_bs_btn/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_bs_btn_domain_model_hivecptcntbsbtnrender.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntbsbtn_hivecptcntbsbtnrender
                        }
                    }
                    hivecptcntbsbtndocumentation {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_cpt_cnt_bs_btn') . 'Resources/Public/Icons/user_plugin_hivecptcntbsbtndocumentation.svg
                        title = LLL:EXT:hive_cpt_cnt_bs_btn/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_bs_btn_domain_model_hivecptcntbsbtndocumentation
                        description = LLL:EXT:hive_cpt_cnt_bs_btn/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_bs_btn_domain_model_hivecptcntbsbtndocumentation.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntbsbtn_hivecptcntbsbtndocumentation
                        }
                    }
                }
                show = *               
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function($extKey)
    {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hivecptcntbsbtnrender >
                wizards.newContentElement.wizardItems.plugins.elements.hivecptcntbsbtndocumentation >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hivecptcntbsbtnrender {
                            iconIdentifier = bootstrap_cpt_brand_32x32_svg
                            title = Bootstrap Button
                            description = Button(s) without Flux
                            tt_content_defValues {
                                CType = list
                                list_type = hivecptcntbsbtn_hivecptcntbsbtnrender
                            }
                        }
                        show := addToList(hivecptcntbsbtnrender)
                    }

                }
            }'
        );

    }, $_EXTKEY
);
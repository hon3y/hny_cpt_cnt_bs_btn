<?php
namespace HIVE\HiveCptCntBsBtn\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class BtnTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveCptCntBsBtn\Domain\Model\Btn
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveCptCntBsBtn\Domain\Model\Btn();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getBackendTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBackendTitle()
        );

    }

    /**
     * @test
     */
    public function setBackendTitleForStringSetsBackendTitle()
    {
        $this->subject->setBackendTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'backendTitle',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );

    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function getAlternativeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAlternative()
        );

    }

    /**
     * @test
     */
    public function setAlternativeForStringSetsAlternative()
    {
        $this->subject->setAlternative('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'alternative',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function getValueReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getValue()
        );

    }

    /**
     * @test
     */
    public function setValueForStringSetsValue()
    {
        $this->subject->setValue('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'value',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function getLinkReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLink()
        );

    }

    /**
     * @test
     */
    public function setLinkForStringSetsLink()
    {
        $this->subject->setLink('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'link',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function getClassColorReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getClassColor()
        );

    }

    /**
     * @test
     */
    public function setClassColorForStringSetsClassColor()
    {
        $this->subject->setClassColor('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'classColor',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function getClassSizeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getClassSize()
        );

    }

    /**
     * @test
     */
    public function setClassSizeForStringSetsClassSize()
    {
        $this->subject->setClassSize('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'classSize',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function getClassAdditionalReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getClassAdditional()
        );

    }

    /**
     * @test
     */
    public function setClassAdditionalForStringSetsClassAdditional()
    {
        $this->subject->setClassAdditional('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'classAdditional',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function getClassGlyphiconReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getClassGlyphicon()
        );

    }

    /**
     * @test
     */
    public function setClassGlyphiconForStringSetsClassGlyphicon()
    {
        $this->subject->setClassGlyphicon('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'classGlyphicon',
            $this->subject
        );

    }

    /**
     * @test
     */
    public function getJustifyReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getJustify()
        );

    }

    /**
     * @test
     */
    public function setJustifyForBoolSetsJustify()
    {
        $this->subject->setJustify(true);

        self::assertAttributeEquals(
            true,
            'justify',
            $this->subject
        );

    }
}
